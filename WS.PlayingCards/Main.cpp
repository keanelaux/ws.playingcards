

// Playing Cards
// William Schweitzer

#include <iostream>
#include <conio.h>

using namespace std;



enum Rank { TWO = 2, THREE = 3, FOUR = 4, FIVE = 5, SIX = 6, SEVEN = 7, EIGHT = 8, NINE = 9, TEN = 10, JACK = 11, QUEEN = 12, KING = 13, ACE = 14 };
enum Suit { SPADES, DIAMONDS, CLUBS, HEARTS };
struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);


int main()
{
	// testing
	Card card1;
	card1.Rank = QUEEN;
	card1.Suit = DIAMONDS;

	Card card2;
	card2.Rank = QUEEN;
	card2.Suit = SPADES;

	PrintCard(card1);
	HighCard (card1, card2);

	(void)_getch();
	return 0;
}


void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case 2: cout << "The Two of ";
		break;
	case 3: cout << "The Three of ";
		break;
	case 4: cout << "The Four of ";
		break;
	case 5: cout << "The Five of ";
		break;
	case 6: cout << "The Six of ";
		break;
	case 7: cout << "The Seven of ";
		break;
	case 8: cout << "The Eight of ";
		break;
	case 9: cout << "The Nine of ";
		break;
	case 10: cout << "The Ten of ";
		break;
	case 11: cout << "The Jack of ";
		break;
	case 12: cout << "The Queen of ";
		break;
	case 13: cout << "The King of ";
		break;
	case 14: cout << "The Ace of ";
		break;
	}

	switch (card.Suit)
	{
	case 0: cout << "Spades";
		break;
	case 1: cout << "Diamonds";
		break;
	case 2: cout << "Clubs";
		break;
	case 3: cout << "Hearts";
		break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}